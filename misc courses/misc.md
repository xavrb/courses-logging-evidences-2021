# Misc courses  


A set of courses not necessarily related to DevOps, infra or agile that are also valuable to my carreer path and knowledge of IT/Tech.  

## Confluence

* **[Completed: 24 April]** [Atlassian Confluence Fundamentals for Beginners 2021, Vlajko Knezic, Udemy](https://www.udemy.com/course/confluence-fundamentals-for-beginners/), [Evidence ](./screenshots/confluence_udemy/)

## JavaScript

* __Project__ **[Completed, 20 FEB]** [Introduction to Javascript: The Basics - Khaled M. Attia - Coursera ](https://www.coursera.org/learn/intro-to-javascript-the-basics/), [Evidence](https://www.coursera.org/account/accomplishments/verify/8BP3AK4G3BZN)

## GO

* **[Completed: 25 Feb]** [Curso de Programación en GO - 2015, Verónica López, Platzi](https://platzi.com/clases/go-basico-2015/), [Evidence ](https://platzi.com/p/javiermejiaperez/curso/1028-go-basico-2015/diploma/detalle/)


## POSTMAN

* **[Completed: 04 March]** [Curso de Postman, Eduardo Álvarez, Platzi](https://platzi.com/clases/postman/), [Evidence ](https://platzi.com/p/javiermejiaperez/curso/1765-postman/diploma/detalle/)



## REGEX

* **[Completed: 28 March]** [Curso de Expresiones Regulares, Alberto Alcocer, Platzi](https://platzi.com/clases/expresiones-regulares/), [Evidence ](https://platzi.com/p/javiermejiaperez/curso/1301-expresiones-regulares/diploma/detalle/)

### Non-Tech  
## Personal Branding   
* **[Completed: 30 March]** [Curso de Marca Personal (Personal branding course), Freddy Vega, Platzi](https://platzi.com/clases/marca-personal/), [Evidence ](https://platzi.com/p/javiermejiaperez/curso/1220-course/diploma/detalle/)
