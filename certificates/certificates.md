# Certificates

## Courses  

* [Python: Aprender a programar, Leonardo Salomn, EDX](https://learning.edx.org/course/course-v1:UPValenciaX+PY101x+2T2020/), [Certificate](https://courses.edx.org/certificates/fe69fd7f39a44d6a80eef5e01ce9117a)
* [Curso de Programación en GO - 2015, Verónica López, Platzi](https://platzi.com/clases/go-basico-2015/), [Certificate)](https://platzi.com/p/javiermejiaperez/curso/1028-go-basico-2015/diploma/detalle/)
* [Curso Profesional de Git y GitHub (Professional course on Git and Github), F. Vega, Platzi](https://platzi.com/clases/git-github/), [Certificate](https://platzi.com/p/javiermejiaperez/curso/1557-git-github/diploma/detalle/)
* [Curso Básico de Python (Basic course on Python), Facundo García Martoni, Platzi](https://platzi.com/clases/python/), [Certificate](https://platzi.com/p/javiermejiaperez/curso/1937-course/diploma/detalle/)
* [Curso de Docker, Guido Vilariño, Platzi](https://platzi.com/clases/docker/), [Certificate](https://platzi.com/p/javiermejiaperez/curso/2066-course/diploma/detalle/)
* [Fundamentos de Docker 2018, Guido Vilariño, Platzi](https://platzi.com/clases/docker-2018/), [Certificate](https://platzi.com/p/javiermejiaperez/curso/1432-docker-2018/diploma/detalle/)
* [Curso de Postman, Eduardo Álvarez, Platzi](https://platzi.com/clases/postman/), [Certificate](https://platzi.com/p/javiermejiaperez/curso/1765-postman/diploma/detalle/)  
* [Agile with Atlassian Jira, Steve Byrnes, Coursera](https://www.coursera.org/learn/agile-atlassian-jira/), [Certificate](https://www.coursera.org/account/accomplishments/verify/ZQHL5G98Z522)
* [Introduction to Containers, Kubernetes, and OpenShift](https://learning.edx.org/course/course-v1:IBM+CC0201EN+3T2020/home), [Certificate](https://courses.edx.org/certificates/858de0684f2b44cdb4bb93e10528bff4)   
* [Curso Profesional de Scrum, Gerardo Romero, Platzi](https://platzi.com/clases/scrum/), [Certificate](https://platzi.com/p/javiermejiaperez/curso/1750-scrum/diploma/detalle/)
* [Google Cloud Platform Fundamentals: Core Infrastructure, Google Cloud, Coursera](https://www.coursera.org/learn/gcp-fundamentals/), [Certificate](https://www.coursera.org/account/accomplishments/verify/DUSR3EQBXN3B)   
* [Curso de Introducción a Elasticsearch, Kevin Sarmiento Mendoza, Platzi](https://platzi.com/clases/intro-elasticsearch/), [Certificate](https://platzi.com/p/javiermejiaperez/curso/1912-course/diploma/detalle/)   
* [Crash Course on Python, Google, Coursera](https://www.coursera.org/learn/python-crash-course), [Evidence](https://www.coursera.org/account/accomplishments/verify/FVPKUEHY5MNU)
* [Curso de Expresiones Regulares, Alberto Alcocer, Platzi](https://platzi.com/clases/expresiones-regulares/), [Evidence ](https://platzi.com/p/javiermejiaperez/curso/1301-expresiones-regulares/diploma/detalle/)
* [Developing a Google SRE Culture, Google Cloud, Coursera](https://www.coursera.org/learn/developing-a-google-sre-culture), [Evidence](https://www.coursera.org/account/accomplishments/verify/HVWY27ZJ2Q4G)   
* [Curso Intermedio de Python (Intermediate course on Python), Facundo García Martoni, Platzi](https://platzi.com/clases/python-intermedio/), [Evidence](https://platzi.com/p/javiermejiaperez/curso/2255-python-intermedio/diploma/detalle/)
* [Linux servers admin, Jhon Edison Castro, Platzi](https://platzi.com/clases/linux/), [Evidence](https://platzi.com/p/javiermejiaperez/curso/1667-linux/diploma/detalle/)   
* [Linux servers admin (2017), Gustavo Angulo, Platzi](https://platzi.com/clases/linux-2017/), [Evidence](https://platzi.com/p/javiermejiaperez/curso/1123-linux-2017/diploma/detalle/)   

## Projects  

* [Create Python Linux Script to Generate a Disk Usage Report - David Dalsveen - Coursera ](https://www.coursera.org/learn/python-linux-script-disk-usage-report/), [Certificate](https://www.coursera.org/account/accomplishments/verify/UNLKGRQXS8DA)  
* [Introduction to Javascript: The Basics - Khaled M. Attia - Coursera](https://www.coursera.org/learn/intro-to-javascript-the-basics/), [Certificate](https://www.coursera.org/account/accomplishments/verify/8BP3AK4G3BZN)
* [Docker fundamentals - Anju M Dominic - Coursera ](https://www.coursera.org/projects/docker-fundamentals), [Certificate](https://www.coursera.org/account/accomplishments/verify/UNBDENV5Y7B8)  
* [Git + GitHub for Open Source Collaboration](https://www.coursera.org/projects/git-and-github), [Certificate](https://www.coursera.org/account/accomplishments/verify/3BEUHTTMGSKY)  
* [Containerize a full-stack NodeJS application in Docker](https://www.coursera.org/learn/containerize-full-stack-nodejs-application-in-docker/), [Certificate](https://www.coursera.org/account/accomplishments/verify/MCNYL9S3YAFZ)
* [Container Orchestration using Kubernetes, Anju M Dominic, Coursera](hhttps://www.coursera.org/learn/container-orchestration-kubernetes/), [Certificate](https://www.coursera.org/account/accomplishments/verify/MUABYWRDJJKQ)
* [Monitoring & Telemetry for Production Systems, Prasanjit Singh, Coursera](https://www.coursera.org/learn/server-application-monitoring/), [Evidence](https://www.coursera.org/account/accomplishments/verify/FFXKHAPDDLN4)  
