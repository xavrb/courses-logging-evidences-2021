# Courses taken between February and June/July 2021  


Latex version [here](./tex_src/readme.pdf).

# Table of contents

1. [Misc Courses](#misc-courses)  
2. [Webinars](#webinars)  
3. [Courses and projects](#courses-and-projects)  
  * [Docker](#docker), [Started, versed]
  * [Kubernetes](#k8s), [Started, versed]
  * [Python](#python), [Well versed]
  * [OCI](#oci), [Need to start] -> [Taken GCP to extrapolate]
  * [Grafana](#grafana), [Taken some courses, need to dive deeper]
  * [ELK stack](#elastic-stack), [Taken some courses, need to dive deeper]
4. [Books](#books)  
5. [Certificates](#certificates)  
6. [Coding Challenges](#coding-challenges)  


A set of courses taken in order to be better prepared as of April 2021 -- infrastructure/devops oriented. A screenshot will be added in case a certificate is not available to share.    

## Misc Courses
A set of courses not necessarily related to DevOps, infra or agile that are also valuable to my carreer path and knowledge of IT/Tech. Were also taken on this time frame. Can be seen [here](./misc%20courses/misc.md).





## Courses and projects
### SRE and DevOps  

 **[Completed: 18 March]** [Google Cloud Platform Fundamentals: Core Infrastructure, Google Cloud, Coursera](https://www.coursera.org/learn/gcp-fundamentals/), [Evidence](https://www.coursera.org/account/accomplishments/verify/DUSR3EQBXN3B)    
**[Completed:  04 April]** [Developing a Google SRE Culture, Google Cloud, Coursera](https://www.coursera.org/learn/developing-a-google-sre-culture), [Evidence](https://www.coursera.org/account/accomplishments/verify/HVWY27ZJ2Q4G)   
**[Completed:  04 June]** [Linux servers admin, Jhon Edison Castro, Platzi](https://platzi.com/clases/linux/), [Evidence](https://platzi.com/p/javiermejiaperez/curso/1667-linux/diploma/detalle/)   
**[Completed:  04 June]** [Linux servers admin (2017), Gustavo Angulo, Platzi](https://platzi.com/clases/linux-2017/), [Evidence](https://platzi.com/p/javiermejiaperez/curso/1123-linux-2017/diploma/detalle/)   

### PYTHON
* **[Completed: 27 Feb]** [Curso Básico de Python (Basic course on Python), Facundo García Martoni, Platzi](https://platzi.com/clases/python/), [Evidence  ](https://platzi.com/p/javiermejiaperez/curso/1937-course/diploma/detalle/)
* __Project__ **[Completed, 15 FEB]** [Create Python Linux Script to Generate a Disk Usage Report - David Dalsveen - Coursera ](https://www.coursera.org/learn/python-linux-script-disk-usage-report/), [Evidence](https://www.coursera.org/account/accomplishments/verify/UNLKGRQXS8DA)  
* **[Completed: 18 Feb]** [Python: Aprender a programar, Leonardo Salomn, EDX](https://learning.edx.org/course/course-v1:UPValenciaX+PY101x+2T2020/), [Evidence (edX certificate)](https://courses.edx.org/certificates/fe69fd7f39a44d6a80eef5e01ce9117a)
* **[Completed: 24 March]** [Crash Course on Python, Google, Coursera](https://www.coursera.org/learn/python-crash-course), [Evidence](https://www.coursera.org/account/accomplishments/verify/FVPKUEHY5MNU)
* **[Completed: 12 April]** [Curso Intermedio de Python (Intermediate course on Python), Facundo García Martoni, Platzi](https://platzi.com/clases/python-intermedio/), [Evidence](https://platzi.com/p/javiermejiaperez/curso/2255-python-intermedio/diploma/detalle/)
<!--* _To be taken_ https://www.coursera.org/learn/python-crash-course, [Evidence](./screenshots/screen1.jpg)  
* _To be taken_ https://www.coursera.org/specializations/python-3-programming, [Evidence](./screenshots/screen1.jpg)  
* _To be taken_ https://www.coursera.org/specializations/programming-python-java, [Evidence](./screenshots/screen1.jpg)    -->



### DOCKER

* __Project__ **[Completed, 14 FEB]** [Docker fundamentals - Anju M Dominic - Coursera ](https://www.coursera.org/projects/docker-fundamentals), [Evidence](https://www.coursera.org/account/accomplishments/verify/UNBDENV5Y7B8)  
* __Project__ **[Completed, 04 March]** [Containerize a full-stack NodeJS application in Docker](https://www.coursera.org/learn/containerize-full-stack-nodejs-application-in-docker/), [Evidence](https://www.coursera.org/account/accomplishments/verify/MCNYL9S3YAFZ)  
* **[Completed: 3 March]** [Fundamentos de Docker 2018, Guido Vilariño, Platzi](https://platzi.com/clases/docker-2018/), [Evidence  ](https://platzi.com/p/javiermejiaperez/curso/1432-docker-2018/diploma/detalle/)
* **[Completed: 3 March]** [Curso de Docker, Guido Vilariño, Platzi](https://platzi.com/clases/docker/), [Evidence  ](https://platzi.com/p/javiermejiaperez/curso/2066-course/diploma/detalle/)
* **[Completed: 08 March]** [Introduction to Containers, Kubernetes, and OpenShift, edX](https://learning.edx.org/course/course-v1:IBM+CC0201EN+3T2020/home), [Evidence](https://courses.edx.org/certificates/858de0684f2b44cdb4bb93e10528bff4)  
<!-- * _To be taken_ https://www.coursera.org/projects/docker-container-essentials-web-app, [Evidence](./screenshots/screen1.jpg)  
* _To be taken_ https://www.coursera.org/projects/introduction-to-docker-build-portfolio-site, [Evidence](./screenshots/screen1.jpg)   -->


### K8S
* **[Completed: 08 March]** [Introduction to Containers, Kubernetes, and OpenShift, edX](https://learning.edx.org/course/course-v1:IBM+CC0201EN+3T2020/home), [Evidence](https://courses.edx.org/certificates/858de0684f2b44cdb4bb93e10528bff4)  
* __Project__ **[Completed: 13 March]** [Container Orchestration using Kubernetes, Anju M Dominic, Coursera](https://www.coursera.org/learn/container-orchestration-kubernetes/), [Evidence](https://www.coursera.org/account/accomplishments/verify/MUABYWRDJJKQ)
<!-- * _To be taken_ https://www.edx.org/es/course/introduction-to-kubernetes, [Evidence](./screenshots/screen1.jpg)  
* _To be taken_ https://www.edx.org/es/course/fundamentals-of-containers-kubernetes-and-red-hat, [Evidence](./screenshots/screen1.jpg)  
* _To be taken_ https://www.coursera.org/projects/container-orchestration-kubernetes, [Evidence](./screenshots/screen1.jpg)     -->


### OCI
 <!--
* Jak'obczyk, M. (2020). Practical Oracle Cloud Infrastructure: Infrastructure as a Service, Autonomous Database, Managed Kubernetes, and Serverless. Apress.
 -->

### GRAFANA  

* __Project__ **[Completed: 15 April]** [Monitoring & Telemetry for Production Systems, Prasanjit Singh, Coursera](https://www.coursera.org/learn/server-application-monitoring/), [Evidence](https://www.coursera.org/account/accomplishments/verify/FFXKHAPDDLN4)  

### ELASTIC STACK  

* <!--  _To be taken_ https://www.coursera.org/projects/processing-and-visualizing-logs-with-elastic-stack, [Evidence](./screenshots/screen1.jpg)  -->
* **[Completed: 19 March]** [Curso de Introducción a Elasticsearch, Kevin Sarmiento Mendoza, Platzi](https://platzi.com/clases/intro-elasticsearch/), [Evidence](https://platzi.com/p/javiermejiaperez/curso/1912-course/diploma/detalle/)    
* __Project__ **[Completed: 15 April]** [Monitoring & Telemetry for Production Systems, Prasanjit Singh, Coursera](https://www.coursera.org/learn/server-application-monitoring/), [Evidence](https://www.coursera.org/account/accomplishments/verify/FFXKHAPDDLN4)  



### GIT/GITHUB

* **[Completed: 26 Feb]** [Curso Profesional de Git y GitHub (Professional course on Git and Github), F. Vega, Platzi](https://platzi.com/clases/git-github/), [Evidence  ](https://platzi.com/p/javiermejiaperez/curso/1557-git-github/diploma/detalle/)
* __Project__ **[Completed: 28 Feb]**  [Git + GitHub for Open Source Collaboration](https://www.coursera.org/projects/git-and-github), [Evidence](https://www.coursera.org/account/accomplishments/verify/3BEUHTTMGSKY)  
<!--* _To be taken_ https://www.coursera.org/specializations/oss-development-linux-git, [Evidence](./screenshots/screen1.jpg)  
* _To be taken_ https://www.coursera.org/learn/version-control-with-git, [Evidence](./screenshots/screen1.jpg)     -->


### AGILE   

* **[Completed: 27 Feb]** [Agile with Atlassian Jira, Steve Byrnes, Coursera](https://www.coursera.org/learn/agile-atlassian-jira/), [Evidence](https://www.coursera.org/account/accomplishments/verify/ZQHL5G98Z522)  
* **[Completed: 10 Mar]** [Curso Profesional de Scrum, Gerardo Romero, Platzi](https://platzi.com/clases/scrum/), [Evidence](https://platzi.com/p/javiermejiaperez/curso/1750-scrum/diploma/detalle/)


### TERRAFORM  

<!--  * _To be taken_ https://www.coursera.org/projects/aprendiendo-terraform, [Evidence](./screenshots/screen1.jpg)  
* _To be taken_ https://www.coursera.org/projects/terraform-devops-aws-cloud-iac-ec2, [Evidence](./screenshots/screen1.jpg)    
-->
### AWS    

<!-- * _To be taken_ https://www.coursera.org/projects/introduction-to-amazon-web-services-aws, [Evidence](./screenshots/screen1.jpg)   -->

## Books

They may be related to either devops/logging or agile methodologies.  

### Coding and scripting
* Lubanovic, B., & Safari, a. (2019). Introducing Python, 2nd Edition. O'Reilly Media, Incorporated.  
* Donovan, A., & Kernighan, B. (2015). The Go Programming Language. Pearson Education.




### Devops and infrastructure
* __[Reading]__ Freeman, E. (2019). DevOps For Dummies. Wiley.
* Kane, S., & Matthias, K. (2018). Docker: Up & Running: Shipping Reliable Containers in Production. O'Reilly Media.  
* Kim, G., Humble, J., Debois, P., & Willis, J. (2016). The DevOps Handbook:: How to Create World-Class Agility, Reliability, and Security in Technology Organizations. IT Revolution Press.  
* Brikman, Y. (2019). Terraform: Up & Running: Writing Infrastructure as Code. O'Reilly Media.  
* __[Reading]__ Young, A. (2019). Infrastructure as Code: A Comprehensive Guide to Managing Infrastructure as Code. Independently Published.



#### OCI   
* Jak\'obczyk, M. (2020). Practical Oracle Cloud Infrastructure: Infrastructure as a Service, Autonomous Database, Managed Kubernetes, and Serverless. Apress.



### Logging and monitoring
* Salituro, E. (2020). Learn Grafana 7.0: A beginner's guide to getting well versed in analytics, interactive dashboards, and monitoring. Packt Publishing.  
* Ligus, S. (2012). Effective Monitoring and Alerting: For Web Operations. O'Reilly Media.  



### Agile methodologies

* Rigby, D., Elk, S., & Berez, S. (2020). Doing Agile Right: Transformation Without Chaos. Harvard Business Review Press.  
* __[Reading]__ Sutherland, J. (2016). Scrum: El arte de hacer el doble de trabajo en la mitad de tiempo. Océano.  



## WEBINARS  

### Docker   
* **[Completed, 12 FEB]** [Docker 101, Jenny Fong -  Docker EE](https://www.youtube.com/watch?v=V9IJj4MzZBc), [Notes](https://web.tresorit.com/l/KDHeV#kqeb-eLLsZ_uL3JOmKV6yQ)   
* **[Completed, 12 FEB]** [Introducción a Docker, Daniel Díaz Carrete - Softtek](https://blog.softtek.com/es/webinar-introducci%C3%B3n-a-docker), [Notes](https://web.tresorit.com/l/knKFe#TINBch02SO1ujkcpPG-VWw)

### Grafana   
* **[Completed, 11 FEB]** [Getting Started with grafana -  Marcus olsson -  Grafana Labs ](https://grafana.com/go/webinar/getting-started-with-grafana/), [Notes](https://web.tresorit.com/l/WSxBI#_9JvgBNCtkXtIm62FlVHGQ)  





## Certificates

Earned certificates can be checked [here](./certificates/certificates.md).




## Coding Challenges  

Coding challenges taken from Aziz, A., Lee, T., & Prakash, A. (2019). Elements of Programming Interviews in Python. EPI.  


[Challenges](./codingChallenges/cchallenges.md)
